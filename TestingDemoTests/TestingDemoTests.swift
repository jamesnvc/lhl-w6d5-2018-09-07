//
//  TestingDemoTests.swift
//  TestingDemoTests
//
//  Created by James Cash on 07-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import XCTest
@testable import TestingDemo

class TestingDemoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(Math.fib(5), 8)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            Math.fib(40)
        }
    }
    
}
