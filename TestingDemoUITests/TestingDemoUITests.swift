//
//  TestingDemoUITests.swift
//  TestingDemoUITests
//
//  Created by James Cash on 07-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import XCTest

class TestingDemoUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let app = XCUIApplication()
        XCTAssertEqual("First thing", app.staticTexts["controllerTitle"].label)
        app/*@START_MENU_TOKEN@*/.buttons["goToNextButton"]/*[[".buttons[\"go to two button\"]",".buttons[\"goToNextButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertEqual("Number Two", app.staticTexts["controllerTitle"].label)
        app/*@START_MENU_TOKEN@*/.buttons["goToNextButton"]/*[[".buttons[\"go to three button\"]",".buttons[\"goToNextButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertEqual("Third View", app.staticTexts["controllerTitle"].label)

        let backButton = app.navigationBars["UIView"].buttons["Back"]
        backButton.tap()

        XCTAssertEqual("Number Two", app.staticTexts["controllerTitle"].label)

        let controllertitleStaticText = app/*@START_MENU_TOKEN@*/.staticTexts["controllerTitle"]/*[[".staticTexts[\"controller title\"]",".staticTexts[\"controllerTitle\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        controllertitleStaticText.tap()
        backButton.tap()

        XCTAssertEqual("First thing", app.staticTexts["controllerTitle"].label)

        controllertitleStaticText.tap()

    }
    
}
