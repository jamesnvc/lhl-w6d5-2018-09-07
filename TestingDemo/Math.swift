//
//  Math.swift
//  TestingDemo
//
//  Created by James Cash on 07-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

struct Math {

    static func naive_fib(_ n: Int) -> Int {
        if n == 0 { return 1 }
        if n == 1 { return 1 }
        return fib(n - 1) + fib(n - 2)
    }

    // this is a particular case of a more general technique called "dynamic programming"
    // in this case, it's often called "memoization"
    // one cavet here is that this cache is essentially a memory leak
    // in general this is something to be careful of with caches
    // more advanced techniques would be to use caches that store a finite number of values (common ones would be TTL (time-to-live) caches, which only keep values for a certain amount of time then remove them or LRU (least-recently-used) caches, which remove values that haven't been looked up recently)
    // for this particular case of fibonacci numbers that's not quite as important, because the size of the cache grows linearlly & the lower values are always used
    // so if you're really worried about memory usage (although you should profile it first!), a better idea might be to just make this instance variables/methods instead of static ones, then the cache just lives as long as the math struct you create does
    static var cache = [0:1, 1:1]
    static func memoized_fib(_ n: Int) -> Int {
        if let f = cache[n] {
            return f
        }
        let f = fib(n - 1) + fib(n - 2)
        cache[n] = f
        return f
    }

    static func fib(_ n: Int) -> Int {
        let m = Double(n + 1)
        let sqrt5 = sqrt(5)
        let phi = (1 + sqrt5)/2
        let psi = 1 - phi
        return Int((pow(phi, m) - pow(psi, m)) / sqrt5)
    }

}
